from django.db import models

# Create your models here.
class Recipe(models.Model):
    pass

# update models:
    #1. update the model/ create the model
    #2. make migrations: python manage.py makemigrations


class Tag(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField("Recipe", related_name="tags")

    def __str__(self):
        return self.name
